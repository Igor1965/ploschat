package com.company;

public class Tri extends Shape {

    private double a;
    private double h;

    public Tri(double a, double h) {
        this.a = a;
        this.h = h;
    }

    @Override
    public double pl(){
         double s;
         s=0.5*(double)a * (double)h;
         return s;
    }

}
