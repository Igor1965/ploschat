package com.company;

public class Kvad extends Shape {

    private double a;

    public Kvad(double a) {
        this.a = a;
    }

    @Override
    public double pl() {
        return this.a * this.a;
    }

}
